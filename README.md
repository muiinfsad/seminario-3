# Seminario 3
 Para un correcto funcionamiento de la aplicación, será necesario ejecutar antes el comando ```npm install```
 
 Este repositorio se compone de 3 carpetas : **s2**, **serviceCart** y **serviceDB**.
 La carpeta s2 contiene el registro de servicios, serviceCart contiene el servicio del carrito y seviceDB el servicio de base de datos.
 Se han llevado a cabo todas las actividades propuestas en este seminario, obligatorias y opcionales.
 Antes de ejecutar los servicios se debería ejectura el archivo db.js, que se encuentra en "sem03/serviceDB/server", con ```node db.js```. Esto añadirá los productos que se especifican en el fichero a la base de datos,
 en este caso se añadirán 2 productos, uno llamado manzana y otro mandarina.
 Para poner en funcionamiento el sistema se debe ejecutar la orden ```npm start``` en las 3 carpetas. Una vez puesto en funcionamiento se puede usar Postman parar realiazar peticiones al servicio del carrito. Con GET se obtiene el carrito, con
 POST se puede añadir un producto al carrito, dado que se hace la comprobación de stock solo serán añadidos los productos que estén en la base de datos y tengan suficiente stock disponible, y con DELETE se pueden eliminar productos del carrito.
 Tras añadir un producto al carrito se puede comprobar si está dentro con GET.  
 Ejemplos:  
 "POST localhost:34871/cart/mandarina": añade el producto mandarina al carrito  
 "GET localhost:34871/cart": obtiene el contenido del carrito  
 "DELETE localhost:34871/cart/mandarina": añade el producto mandarina al carrito

 El servicio del carrito y el servicio de base de datos cuando se lanzan se registran en el registro de servicios, y cuando se interrumpe el proceso con Ctrl+c los dos servicios envían una petición DELETE al registro de servicios para que les borre.
 Todos los servicios ejecutan un servidor usando Express que atiende peticiones REST. Se usa el módulo axios para hacer peticiones desde el código, estas peticiones se usan para registrar a los servicios, para eliminarlos cuando se cieeran y para 
 buscarlos. Cuando el servicio del carrito quiere realizar una consulta a la base de datos se comunica con el registro de servios mendiante una petición REST para obtener la dirección de un servicio de base de datos que esté activo.
 
 
 
