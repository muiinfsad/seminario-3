const axios = require('axios');

exports.addToCart = function (itemName, cart) {
    axios.get(`http://localhost:3000/find/db-service/1.0.0/`).then((response) => {
        axios.get(`http://localhost:` + response.data.port + `/dbContent/${itemName}/`).then((response) => {
            if (response.data != null) {
                cart.push(response.data);
                console.log("Added");
            }

        }, (error) => { console.log("Can't add this item."); });
    })
}

exports.removeFromCart = function (itemName, cart) {
    var removedItem = null;
    for (var itemIndex = 0; itemIndex < cart.length; itemIndex++) {
        if (cart[itemIndex].name == itemName) {
            cart.splice(itemIndex, 1);
            console.log(cart);
            console.log("Removed");
        }
    }
}
